#ifndef A2_INDEX_H
#define A2_INDEX_H
#include "a2.h"

typedef struct doc_entry{
	int doc_id;
	int word_freq;
} doc_entry;

typedef struct hash_bucket{
	int entries;
	char * keyword;
	doc_entry ** e_array;
	struct hash_bucket * next_bucket;
} hash_bucket;

typedef struct hash_value{
	int buckets;
	hash_bucket * first_bucket;
} hash_value;

typedef struct index_table{
	int size;
	hash_value ** table;
} index_table;


/* Function to initialise inverted index.
 * Creates a hash table with a constant size, then 
 * initialises all values to null. */
struct index_table * create_table();

/* Inserts a doc node into the index.
 * Uses hash table insertion code from assignment 1.
 * Buckets at each hash value are sorted by keyword.
 * If a doc ID in a keyword bucket is already found, the frequency for
 * that doc node is increased by one, instead of inserting a new node.*/
struct index_table * index_insert(index_table * i_table, page_array_ptr p_array, 
	doc_entry * new_entry, char * keyword);

/* Function to search for the common URLs among a set of given terms.
 * Keep an array of buckets, with each being the bucket of a corresponding
 * search term. Also keep an array of pointers, to keep track of the current
 * position within these bucket's doc array. If a word is not found, return
 * immediately.
 * Then, starting at the start of all buckets doc arrays, compare the current
 * ID's against each other. If they all match, there is a common URL and it is
 * added to the list of search results. All array pointers are then incremented by one.
 * Otherwise, the bucket current position with the largest rank (earliest in its array)
 * is then incremented by one. Once one bucket is at the end of its array, stop the search
 * and print out all the found URLS. As the bucket arrays are sorted in rank order already,
 * no sorting of the results needs to be done, and they are printed. */
void search_terms(index_table * i_table, page_array_ptr p_array, char ** terms, int terms_len);

/* Function for sorting all bucket arrays in the index.
 * Iterates through all hash values and sorts each of their bucket's doc array.
 * Sorting is done using a quicksort */
struct index_table * sort_arrays(index_table * i_table, page_array_ptr p_array);

/* Function to sort a given doc array.
 * Uses quicksort code from assignment 1.
 * Sorts IDs based on their pagerank. */
void quicksort(doc_entry *** d_array, page_array_ptr p_array, int left, int right);

/* Function to swap two doc nodes in an array */
void swap_docs(doc_entry ** node1, doc_entry ** node2);

/* Function to generate a hash value.
 * Uses hash function from assignment 1.
 * This is Kernighan and Ritchie's Function. */
int hash_gen(char * term);

void sort_search_results(int ** common, page_array_ptr p_array, int left, int right);

/* Function to free memory for inverted index. */
void free_index(index_table * i_table);
#endif
