#ifndef A2_PAGES_H
#define A2_PAGES_H
#include "a2.h"

typedef struct temp_doc {
	char * keyword;
	char * url;
} temp_doc;

typedef struct docs_array {
	int count;
	temp_doc ** array;
} docs_array;

typedef struct temp_node {
	int t_id;
	char * url;
} temp_node;

typedef struct name_array {
	int count;
	temp_node ** array;
} name_array;


/* Function to initialise name sorted array.
 * Copies over all values from pagerank array */
struct name_array * init_array(page_array_ptr p_array);

/* Function to initialise temporary doc array */
struct docs_array * init_docs();

/* Function to add new doc to array.
 * Allocates space for another spot in array any adds new doc to the end of
 * the array. */
void new_doc(docs_array_ptr * d_array, temp_doc * new_doc);

/* Function to quicksort name array by URL name.
 * Function is the quicksort function used in assignment 1. Compares URLs
 * values against each other and sorts array from lowest to highest. */
void sort_array(name_array_ptr * u_array, int min, int max);

/* Function to swap two nodes in an array. Used in quicksort.
 * Swaps the pointers of two positions in an array */ 
void swap_nodes(temp_node ** node1, temp_node ** node2);

/* Function to find a given URL in the names array. 
 * Uses the binary search function from assignment 1.
 * Returns the ID of the given URL, or -1 if it is not found. */
int find_url(name_array_ptr u_array, char *s_url, int max);

/* Function to place all the doc frequency recordings into the inverted index. 
 * Function creates a new doc frequency element for the index. It then takes the
 * URL given by the temp node and searches for it. If the ID is found, it is 
 * assigned to the new node. Otherwise, the node is not inserted into the array.
 * The node is inserted into the index and the temp node is freed. */
struct index_table * insert_terms(name_array_ptr u_array, docs_array_ptr d_array, index_table_ptr i_table, page_array_ptr p_array);

/* Function to free names array */
void free_names(name_array * n_array);

#endif
