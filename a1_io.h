#ifndef A2_IO_H
#define A2_IO_H
#include "a2.h"

void read_pages(char * filename);
char * read_char_input();
void read_rest_of_line();

#endif
