#include "a2_io.h"

struct page_array * read_pages(char * filename, page_array * p_array, docs_array * d_array)
{
	FILE * fp;
	int page_key, previous_id = -1, position;
	char * tempstring, * line_token;
	int * count_ptr, count = 0, i;
	char * end_ptr;
	char * temp_key;
	char * page_name;
	char * page_link;
	char * link_keyword;
	char ** keywords;
	char * previous_url = "";
	char * kwrd_null = "(null)";
	char attr_delim[2] = "\t";
	temp_doc * tmp_doc = malloc(sizeof(struct temp_doc));
	page_data * new_data = malloc(sizeof(struct page_data));
	count_ptr = &count;

	/* Open file */
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE);
	assert(tempstring != NULL);

	/* Read lines from file */
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		*count_ptr = 0;
		tempstring[strlen(tempstring) - 1] = '\0';
		line_token = strtok(tempstring, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading KEY line token.\n");
			exit(EXIT_FAILURE);
		}
		temp_key = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading PAGE NAME line token.\n");
			exit(EXIT_FAILURE);
		}
		page_name = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading PAGE LINK line token.\n");
			exit(EXIT_FAILURE);
		}
		page_link = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token != NULL)
		{
			/* Handling for when no link keyword is given */
			link_keyword = line_token;
			line_token = strtok(NULL, attr_delim);
			if(line_token != NULL)
			{
				fprintf(stderr, "Error reading EOL token\n");
				exit(EXIT_FAILURE);
			}
		}
		else
			link_keyword = NULL;

		/* Convert page ID to integer and move counter down 1 (IDs start
 * 		from 1 in file, but 0 in array */
		page_key = (int) strtol(temp_key, &end_ptr, 10);
		page_key -= 1;

		/* If the new page URL is not the same as the previous */
  		if(strcmp(page_name, previous_url) != 0)
  		{
  			/*Create a new page entry in the pagerank array */
			new_data = malloc(sizeof(struct page_data));
  			new_data->id = page_key;
 			new_data->url = malloc(sizeof(char)*(strlen(page_name)+1));
  			strcpy(new_data->url, page_name);
  			new_data->outbound_urls = malloc(sizeof(char*)*1);
  			new_data->outbound_urls[0] = malloc((strlen(page_link)+1)*sizeof(char));
  			strcpy(new_data->outbound_urls[0], page_link);	
  			new_data->outbound_count = 1;
			new_data->inbound_count = 1;
  			new_data->rank = DEF_RANK;

			/* If no keyword is passed in replace it with '(null)' */
			if(link_keyword == NULL)
			{
				tmp_doc = calloc(1, sizeof(temp_doc));
				tmp_doc->keyword = malloc((strlen(kwrd_null)+1)*sizeof(char));
				strcpy(tmp_doc->keyword, kwrd_null);
				tmp_doc->url = malloc((strlen(page_link)+1)*sizeof(char));
				strcpy(tmp_doc->url, page_link);
				new_doc(&d_array, tmp_doc);
			}
		
			/* Otherwise tokenise keywords and add them to temp docs array */
			else
			{
				keywords = malloc(sizeof(char *));
				tokenise_string(&keywords, link_keyword, count_ptr);
				for(i = 0; i < *count_ptr; i++)
				{
					tmp_doc = calloc(1, sizeof(temp_doc));
					tmp_doc->keyword = malloc((strlen(keywords[i])+1)*sizeof(char));
					strcpy(tmp_doc->keyword, keywords[i]);
					tmp_doc->url = malloc((strlen(page_link)+1)*sizeof(char));
					strcpy(tmp_doc->url, page_link);
					new_doc(&d_array, tmp_doc);
				}
				for(i = 0; i < *count_ptr; i++)
				{
					free(keywords[i]);
				}
			}
  			
			/* Add new page to pagerank array */
  			new_data->outbound_ids = malloc(sizeof(int));
  			new_data->inbound_ids = malloc(sizeof(int));
			new_data->inbound_ids[0] = -1;
			previous_url = NULL;
			previous_url = realloc(previous_url, (strlen(page_name)+1) * sizeof(char));
  			strcpy(previous_url, page_name);
  			previous_id = page_key;
  			p_array = add_page(p_array, new_data);
  		}
		
		/* Otherwise pagerank entry for page URL exists, update it and
 * 		add keywords to temp docs array */
  		else
  		{
  			position = p_array->array[previous_id]->outbound_count;
  			p_array->array[previous_id]->outbound_urls = 
  				realloc(p_array->array[previous_id]->outbound_urls,
  				sizeof(char*)*(position+1));
  			p_array->array[previous_id]->outbound_urls[position] = 
				malloc(sizeof(char)*(strlen(page_link)+1));
  			strcpy(p_array->array[previous_id]->outbound_urls[position], page_link);
  			p_array->array[previous_id]->outbound_count += 1;
		
			/* Adding NULL keyword */
			if(link_keyword == NULL)
			{
				tmp_doc = calloc(1, sizeof(temp_doc));
				tmp_doc->keyword = malloc((strlen(kwrd_null)+1)*sizeof(char));
				strcpy(tmp_doc->keyword, kwrd_null);
				tmp_doc->url = malloc((strlen(page_link)+1)*sizeof(char));
				strcpy(tmp_doc->url, page_link);
				new_doc(&d_array, tmp_doc);	
			}
			else
			{
				/* Adding keywords */
				keywords = malloc(sizeof(char *));
				tokenise_string(&keywords, link_keyword, count_ptr);
				for(i = 0; i < *count_ptr; i++)
				{
					tmp_doc = calloc(1, sizeof(temp_doc));
					tmp_doc->keyword = malloc((strlen(keywords[i])+1)*sizeof(char));
					strcpy(tmp_doc->keyword, keywords[i]);
					tmp_doc->url = malloc((strlen(page_link)+1)*sizeof(char));
					strcpy(tmp_doc->url, page_link);
					new_doc(&d_array, tmp_doc);
				}
				for(i = 0; i < *count_ptr; i++)
				{
					free(keywords[i]);
				}
			}
  		}
  	}
	fclose(fp);
	free(tempstring);
	free(previous_url);
	return p_array;	
}

char ** read_char_input(int * count_ptr)
{
	int finished = FALSE;
	int empty = FALSE;
	char * tempstring, * final_string, ** string_array;
	final_string = malloc(sizeof(char) * MAX_INPUT_SIZE);
	tempstring = malloc(sizeof(char) * MAX_INPUT_SIZE+1);
	string_array = calloc(1, sizeof(char *));
	assert(tempstring != NULL);
	

	/* Prompt for search terms */
	printf("\n\nPlease enter search terms (max 50 characters): ");	
	do
	{
		/* CTRL+D Input checking */
		if(fgets(tempstring, MAX_INPUT_SIZE+1, stdin) == NULL)
		{
			printf("\n");
			memset(tempstring, 0, MAX_INPUT_SIZE+1);
			empty = TRUE;
			finished = TRUE;
		}
		
		/* Large input handling */
		if(tempstring[strlen(tempstring) - 1] != '\n' && tempstring[0] != '\0')
		{
			printf("Input too long.\n");
			read_rest_of_line();
		}
		else
		{
			tempstring[strlen(tempstring) - 1] = '\0';
			finished = TRUE;
		}		
	} while(!finished);

	/* Newling/CTRL+D input handling */
	if(tempstring[0] == '\0' || empty == TRUE)
	{
		free(tempstring);
		free(final_string);
		printf("Empty line entered.\n");
		return NULL;
	}
	strcpy(final_string, tempstring);

	/* Tokenise input */
	tokenise_string(&string_array, final_string, count_ptr);
	free(tempstring);
	free(final_string);
	return string_array;
}

void tokenise_string(char *** words, char * string, int * count)
{
	char * line_token;
	int i;
	*count = 0;
	line_token = strtok(string, " ");
	while(line_token != NULL)
	{
		*count += 1;
		/* Prepare to add new word to words array */
		*words = realloc(*words, sizeof(char *)*(*count));
		(*words)[*count-1] = malloc((strlen(line_token) + 1)* sizeof(char));

		/* Convert word to lowercase */
		for(i = 0; i < strlen(line_token); i++)
		{
			line_token[i] = tolower(line_token[i]);
		}	
		strcpy((*words)[*count-1], line_token);
		line_token = strtok(NULL, " ");
	}
	return;
	
}

void read_rest_of_line()
{
	int ch;
	while(ch=getc(stdin), ch != EOF && ch != '\n')
		;
	clearerr(stdin);
}
