#include "a2_index.h"

struct index_table * create_table()
{
	int i;

	/* Create index */
	index_table * inv_index = calloc(1, sizeof(struct index_table));
	hash_value * h_value;
	inv_index->size = HASH_VALUE;
	inv_index->table = malloc(sizeof(struct hash_value)*HASH_VALUE);

	/* Initialise all index values as null */
	for(i = 0; i < inv_index->size; i++)
	{
		h_value = malloc(sizeof(struct hash_value));
		inv_index->table[i] = h_value;
		inv_index->table[i]->buckets = 0;
		inv_index->table[i]->first_bucket = NULL;
	}
	return inv_index;
}

struct index_table * index_insert(index_table * i_table, page_array * p_array, 
	doc_entry * new_entry, char * new_keyword)
{
	int h_value, j;
	hash_bucket * new_bucket;
	hash_bucket * current_bucket, * prev_bucket;

	if(new_keyword == NULL)
	{
		printf("NULL PASSED AS KEYWORD.\n");
		return NULL;
	}

	/* Generate new keyword's hash value */
	h_value = hash_gen(new_keyword);

	/* If hash value bucket list is empty */
	if(i_table->table[h_value]->first_bucket == NULL)
	{
		new_bucket = calloc(1, sizeof(hash_bucket));
		new_bucket->e_array = calloc(1, sizeof(struct doc_entry) );
		new_bucket->e_array[0] = new_entry;
		new_bucket->entries = 1;
		new_bucket->next_bucket = NULL;
		new_bucket->keyword = malloc((strlen(new_keyword)+1)*sizeof(char));
		strcpy(new_bucket->keyword, new_keyword);
		i_table->table[h_value]->buckets = 1;
		i_table->table[h_value]->first_bucket = new_bucket;
	}
	else
	{
		prev_bucket = NULL;
		current_bucket = i_table->table[h_value]->first_bucket;
		while(current_bucket != NULL)
		{
	
			/* Inserting new bucket before existing bucket */
			if(strcmp(new_keyword, current_bucket->keyword) < 0)
			{
				new_bucket = calloc(1, sizeof(hash_bucket));
				new_bucket->e_array = calloc(1, sizeof(struct doc_entry));
				new_bucket->e_array[0] = new_entry;
				new_bucket->entries = 1;
				new_bucket->next_bucket = current_bucket;
				new_bucket->keyword = malloc((strlen(new_keyword)+1)*sizeof(char));
				strcpy(new_bucket->keyword, new_keyword);
				i_table->table[h_value]->buckets += 1;
				if(prev_bucket == NULL)
				{
					i_table->table[h_value]->first_bucket = new_bucket;
				}
				else
				{
					prev_bucket->next_bucket = new_bucket;
				}
				return i_table;
			}

			/* Current bucket already exists */
			else if(strcmp(current_bucket->keyword, new_keyword) == 0)
			{
				/* Current doc ID in bucket already exists */
				for(j = 0; j < current_bucket->entries; j++)
				{
					if(new_entry->doc_id == current_bucket->e_array[j]->doc_id)
					{
						current_bucket->e_array[j]->word_freq += 1;
						return i_table;
					}
				}

				/* New ID for bucket */
				current_bucket->e_array = realloc(current_bucket->e_array, 
					sizeof(struct doc_entry)*(current_bucket->entries+1));
				current_bucket->e_array[current_bucket->entries] = new_entry;
				current_bucket->entries += 1;
				return i_table;
			}
			prev_bucket = current_bucket;
			current_bucket = current_bucket->next_bucket;
		}

		/* New bucket at end of bucket list */
		new_bucket = malloc(sizeof(hash_bucket));
		new_bucket->e_array = malloc(sizeof(struct doc_entry));
		new_bucket->e_array[0] = new_entry;
		new_bucket->entries = 1;
		new_bucket->next_bucket = NULL;
		new_bucket->keyword = malloc((strlen(new_keyword)+1)*sizeof(char));
		strcpy(new_bucket->keyword, new_keyword);
		i_table->table[h_value]->buckets += 1;
		prev_bucket->next_bucket = new_bucket;
		return i_table;
	}
	return i_table;
}

void search_terms(index_table * i_table, page_array * p_array, char ** terms, int terms_len)
{
	/* Keep pointer for all terms
 * 	Loop through doc_entry arrays for each word
 * 	If current array ids for all words are equal, store that number to print
 * 	Otherwise, increment index of lowest value by one.
 * 	Repeat until you reach end of one list. */
	int * pointers = malloc(sizeof(int)*terms_len);
	hash_bucket ** buckets = malloc(sizeof(hash_bucket)*terms_len);
	hash_bucket * curr_bucket;
	int i, j, curr_hash, curr_max, matching, loop = TRUE, match_count = 0, loop_id, curr_id;
	double top_rank, curr_rank;
	int * common = calloc(1, sizeof(int));
	printf("%d\n", terms_len);

	/* Initialise pointers and buckets array */
	for(i = 0; i < terms_len; i++)
	{
		pointers[i] = 0;
		curr_hash = hash_gen(terms[i]);
		curr_bucket = i_table->table[curr_hash]->first_bucket;
		while(curr_bucket != NULL)
		{
			if(strcmp(curr_bucket->keyword, terms[i]) == 0)
			{
				buckets[i] = curr_bucket;
				break;
			}
			curr_bucket = curr_bucket->next_bucket;
		}
	
		/* Non-existent keyword handling */
		if(curr_bucket == NULL)
		{
			printf("Word %s not found!\n", terms[i]);
			return;
		}
	}
	
	/* Finding common ID's (search results */
	while(loop)
	{
		matching = TRUE;
		curr_max = 0.00;
		top_rank = p_array->array[buckets[0]->e_array[pointers[0]]->doc_id]->rank;
		loop_id = buckets[0]->e_array[pointers[0]]->doc_id;

		/* Comparing current term ID against previous term ID */		
		for(i = 0; i < terms_len; i++)
		{
			curr_id = buckets[i]->e_array[pointers[i]]->doc_id;
			curr_rank = p_array->array[curr_id]->rank;
			if(curr_id != loop_id)
				matching = FALSE;

			/* Handling highest pagerank value */
			if(curr_rank > top_rank)
			{	
				top_rank = curr_rank;
				curr_max = i;
			}	
		}

		/* Incrementing highest current pagerank term */
		if(matching == FALSE)
		{
			pointers[curr_max] += 1;
			if(pointers[curr_max] == buckets[curr_max]->entries)
			{
				loop = FALSE;
			}
		}

		/* Search result found */
		else
		{
			match_count += 1;
			common = realloc(common, sizeof(int)*match_count);
			common[match_count-1] = loop_id;

			/* Increment all pointers from matching ID */
			for(j = 0; j < terms_len; j++)
			{
				pointers[j] += 1;
				if(pointers[j] == buckets[j]->entries)
					loop = FALSE;
			}

		}

		/* End of array met */
		if(pointers[curr_max] == buckets[curr_max]->entries)
			loop = FALSE;
	}

	/* No common URL handling */
	if(match_count == 0)
	{
		printf("No page matches found for given word combination.\n");	
	}
	else
	{
		/* Printing search results */
		for(i = 0; i < match_count; i++)
		{
			printf("Rank %d : %s (Pagerank %0.5f)\n", i+1, p_array->array[common[i]]->url, 
				p_array->array[common[i]]->rank);
		}
	}
	free(common);
	free(pointers);
	free(buckets);
	return;
}

struct index_table * sort_arrays(index_table * i_table, page_array * p_array)
{
	int i;
	hash_bucket * current_bucket;

	/* Access all doc arrays */
	for(i = 0; i < HASH_VALUE; i++)
	{
		current_bucket = i_table->table[i]->first_bucket;
		while(current_bucket != NULL)
		{
			
			/* Sort current array */
			quicksort(&(current_bucket->e_array), p_array, 0, current_bucket->entries - 1);	
			current_bucket = current_bucket->next_bucket;
		}
	}
	return i_table;
}
 
void quicksort(doc_entry *** d_array, page_array * p_array, int left, int right)
{
	int i = left, j = right, curr_id_left, curr_id_right;

	/* Find pivot pagerank */
	int middle = (int)floor((float)((right-left)/2) + left);
	int middle_id = (*d_array)[middle]->doc_id;
	double pivot = p_array->array[middle_id]->rank;

	while(i <= j)
	{

		/* Find first out of place values to left and right of pivot */
		curr_id_left = (*d_array[i])->doc_id;
		curr_id_right = (*d_array[j])->doc_id;
		while(p_array->array[curr_id_left]->rank < pivot)
			i++;
		while(p_array->array[curr_id_right]->rank > pivot)
			j++;
		
		/* Swap nodes */
		if(i <= j)
		{	
			swap_docs(&(*d_array[i]), &(*d_array[j]));
			i++;
			j++;
		}
	};
	
	/* Recursively sort the unsorted sections */
	if(left < j)
		quicksort(d_array, p_array, left, j);
	if(i < right)
		quicksort(d_array, p_array, j, right);
	return;
}

void swap_docs(doc_entry ** node1, doc_entry ** node2)
{

	/* Swap node positions */
	doc_entry * t_node;
	t_node = *node1;
	*node1 = *node2;
	*node2 = t_node;
	return;
}

int hash_gen(char * term)
{
	int init_value = 0;
	long int multiplier = 31;
	int i;
	unsigned int final_hash;
	unsigned long int total_multi;
	total_multi = init_value;

	/* Kernighan and Ritchie's function */
	for (i = 0; i < strlen(term); i++)
	{
		total_multi = multiplier * total_multi + (long int) term[i];
	}
	final_hash = total_multi % HASH_VALUE;
	return final_hash;
}

void sort_search_results(int ** common, page_array * p_array, int left, int right)
{
	int i = left, j = right, tmp_entry;

	int middle = (int)floor((float)((right-left)/2) + left);
	int middle_id = *common[middle];
	double pivot = p_array->array[middle_id]->rank;

	while(i <= j)
	{
		while(p_array->array[(*common[i])]->rank < pivot)
			i++;
		while(p_array->array[(*common[j])]->rank > pivot)
			j++;
		
		if(i <= j)
		{
			tmp_entry = *common[i];
			*common[i] = *common[j];
			*common[j] = tmp_entry;
			i++;
			j++;
		}
	};
	
	if(left < j)
		sort_search_results(common, p_array, left, j);
	if(i < right)
		sort_search_results(common, p_array, j, right);
	return;

}

void free_index(index_table * i_table)
{
	int i, j;
	hash_bucket * curr_bucket, * prev_bucket;

	/* Free all hash values */
	for(i = 0; i < i_table->size; i++)
	{
		prev_bucket = NULL;
		curr_bucket = i_table->table[i]->first_bucket;

		/* Free all buckets */
		while(curr_bucket != NULL)
		{
			/* Free all array sections */
			for(j = 0; j < curr_bucket->entries; j++)
			{
				free(curr_bucket->e_array[j]);
			}
			free(curr_bucket->e_array);
			free(curr_bucket->keyword);
			prev_bucket = curr_bucket;
			curr_bucket = curr_bucket->next_bucket;
			free(prev_bucket);
		}
		free(i_table->table[i]);
	}
	free(i_table->table);
	free(i_table);
}
