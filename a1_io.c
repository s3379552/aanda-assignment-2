#include "a2_io.c"

void read_pages(char * filename)
{
	int page_key, temp_int;
	char * end_ptr;
	char * temp_key;
	char * page_name;
	char * page_link;
	char * link_keyword
	char attr_delim[2] = "\t";
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		exit(EXIT_FAILURE);
	}
	tempstring = (char *) malloc(MAX_LINE);
	assert(tempstring != NULL);
	while (fgets(tempstring, MAX_LINE+1, fp) != NULL)
	{
		tempstring[strlen(tempstring) - 1] = '\0';
		line_token = strtok(tempstring, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading KEY line token.\n");
			exit(EXIT_FAILURE);
		}
		temp_key = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading PAGE NAME line token.\n");
			exit(EXIT_FAILURE);
		}
		page_name = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading PAGE LINK line token.\n");
			exit(EXIT_FAILURE);
		}
		page_link = line_token;
		line_token = strtok(NULL, attr_delim);
		if(line_token != NULL)
		{
			link_keyword = line_token;
			link_token = strtok(NULL, attr_delim);
			if(line_token != NULL)
			{
				fprintf(stderr, "Error reading EOL token\n");
				exit(EXIT_FAILURE);
			}
		}
		else
			link_keyword = NULL;
		page_key = (int) strtol(temp_key, &end_ptr, 10);
		printf("%d\n", page_key);
		
		/* code for inserting into temp structs
 * 		tmp_struct = malloc(sizeof(struct temp_data));
 * 		tmp_struct->name = malloc(sizeof(char)*(strlen(page_name)+1));
 * 		strcpy(tmp_struct->name, page_name);
 * 		tmp_struct->link = malloc(sizeof(char)*(strlen(page_link)+1));	
 * 		strcpy(tmp_struct->link, page_link);
 * 		if(link_keyword != NULL)
 * 		{
 * 			tmp_struct->keyword = malloc(sizeof(char)*(strlen(link_keyword)+1));
 * 			strcpy(tmp_struct->keyword, link_keyword);
 * 		}
 * 		temp_array_insert(tmp_struct, tmp_array);
 * 		*/
	fclose(fp);
	free(tempstring);	
	}
}

char * read_char_input()
{
	int finished = FALSE;
	char * tempstring, * final_string;
	final_string = malloc(sizeof(char) * MAX_INPUT_SIZE);
	tempstring = malloc(sizeof(char) * MAX_INPUT_SIZE+1);
	assert(tempstring != NULL)
	
	do
	{
		if(fgets(tempstring, MAX_INPUT_SIZE+1, stdin) == NULL)
		{
			printf("\n");
			memset(tempstring, 0, MAX_INPUT_SIZE+1);
		}
		
		if(tempstring[strlen(tempstring) - 1] != '\n' && tempstring[0] != '\0')
		{
			printf("Input too long.\n");
			read_rest_of_line();
		}
		else
		{
			tempstring[strlen(tempstring) - 1] = '\0';
			finished = TRUE;
		}		
	} while(!finished);
	strcpy(final_string, tempstring);
	free(tempstring);
	return final_string;
}

void read_rest_of_line()
{
	int ch;
	while(ch=getc(stdin), ch != EOF && ch != '\n')
		;
	clearerr(stdin);
}
