#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#ifndef A2_H
#define A2_H

/* Constant values */
#define MAX_LINE 1000
#define MAX_URL 5000
#define MAX_INPUT_SIZE 50
#define D_FACTOR 0.85
#define HASH_VALUE 5013
#define DEF_RANK 1.00

/* Boolean declaration */
typedef enum truefalse
{
	FALSE, TRUE
} BOOLEAN;

/* pointers for header file function delcarations */
typedef struct page_array * page_array_ptr;
typedef struct docs_array * docs_array_ptr;
typedef struct index_table * index_table_ptr;
typedef struct name_array * name_array_ptr;	

/* Header files */
#include "a2_io.h"
#include "a2_rank.h"
#include "a2_index.h"
#include "a2_pages.h"
#endif
