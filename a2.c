#include "a2.h"

int main(void)
{
	char * pagefile = "graph.txt";
	char ** user_input;
	int * count_ptr;
	int ipt_count = 0;
	page_array * aptr;
	docs_array * dptr;
	name_array * nptr;
	index_table * iptr;
	dptr = init_docs();
	aptr = init_rank_array();
	iptr = create_table();
	count_ptr = &ipt_count;
	printf("NOTE: INITIALISATION CAN TAKE A WHILE, PLEASE BE PATIENT.\n");
	printf("loading pages from file...\n");
	aptr = read_pages(pagefile, aptr, dptr);
	printf("done!\n");
	printf("Adding outbound/inbound id links...");
	aptr = add_outbound_links(aptr);
	printf("done!\n");
	printf("creating name array...\n");
	nptr = init_array(aptr);
	printf("sorting it...\n");
	sort_array(&nptr, 0, nptr->count-1);
	/*for(i = nptr->count-16; i < nptr->count; i++)
	{
		printf("url %d : %s\n", i, nptr->array[i]->url);
	}*/
	printf("done!\n");
	printf("Calculating page rank (NOTE, THIS CAN TAKE SOME TIME)...\n");	
	aptr = calc_page_rank(aptr);
	printf("RMIT HOME OUTBOUND URLS: %d\n", aptr->array[0]->outbound_count);
	printf("done!\n");
	printf("Adding entries to inverted index...\n");
	insert_terms(nptr, dptr, iptr, aptr);
	printf("done!\n");
	user_input = read_char_input(count_ptr);
	while(user_input != NULL)		
	{	
		search_terms(iptr, aptr, user_input, *count_ptr);
		user_input = read_char_input(count_ptr);
	}	
	free_names(nptr);
	free_rank(aptr);
	free_index(iptr);
	printf("program done!\n");
	return EXIT_SUCCESS;	
}
