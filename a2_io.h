#ifndef A2_IO_H
#define A2_IO_H
#include "a2.h"


/* Function for reading text file to pagerank array, and to temporary 
 * doc frequency array.
 * The function first reads the current line from the file, tokenises it
 * for the seperate values. If the new page URL does not match the previously
 * read page URL, then it is a unique URL to add to the pagerank array. Otherwise,
 * the URL is the previously added URL to the array, so update that entry's
 * outbound links accordingly. In all cases, tokenise the line keywords, and add
 * a single node per keyword (with the link url) to the temporary frequency array. */
struct page_array * read_pages(char * filename, page_array_ptr p_array, docs_array_ptr d_array);

/* Function for reading search terms from the user.
 * Continuously prompt the user for a valid (less than 50 characters) string of
 * keywords. Tokenise this string and return the array of words created. If
 * an empty line is entered, return NULL.*/
char ** read_char_input(int * count_ptr);

/* Function for tokenising a given string to a word array.
 * The function tokenises the string, and also converts each word to lowercase
 * lettering. It retuns the updated int pointer for the number of words, as well
 * as the array of words. */
void tokenise_string(char *** words, char *string, int *count);

/* Function for cleaning the buffer on inputs that are too long.*/
void read_rest_of_line();

#endif
