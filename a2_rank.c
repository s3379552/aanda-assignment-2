#include "a2_rank.h"

struct page_array * init_rank_array()
{
	/* Create new pagerank array */
	page_array * p_array = malloc(sizeof(struct page_array));
	p_array->count = 0;
	p_array->array = malloc(sizeof(struct page_data));
	return p_array;
}

struct page_array * add_page(page_array * p_array, page_data * new_page)
{
	/* Increase array size */
	if(p_array->count > 0)
		p_array->array = realloc(p_array->array, sizeof(page_data) *
			(p_array->count+1));
	
	/* Add new URL */
	p_array->array[p_array->count] = new_page;
	p_array->count += 1;
	return p_array;
}

struct page_array * add_outbound_links(page_array * p_array)
{
	page_data * sink_data;
	int i, j, s_id, outbound_size = 0, orig_end;
	orig_end = p_array->count;

	/* for all non rank-sinks */
	for(i = 0; i < orig_end; i++)
	{
		outbound_size = p_array->array[i]->outbound_count;

		/* For all outbound URLS search for its ID */
		for(j = 0; j < p_array->array[i]->outbound_count; j++)
		{
			if(outbound_size > 1)
			{
				p_array->array[i]->outbound_ids = realloc(p_array->array[i]->outbound_ids, 
				sizeof(int)*(outbound_size));
				assert(p_array->array[i]->outbound_ids != NULL);
			}
			s_id = search_id(p_array, p_array->array[i]->outbound_urls[j]);

			
			/* New URL page sink handling */
			if(s_id == -1)
			{
				sink_data = malloc(sizeof(struct page_data));
				sink_data->id = p_array->count;
				sink_data->url = malloc(sizeof(char)*
					(strlen(p_array->array[i]->outbound_urls[j])+1));
				strcpy(sink_data->url, p_array->array[i]->outbound_urls[j]);
				sink_data->outbound_urls = NULL;
				sink_data->inbound_count = 1;
				sink_data->inbound_ids = malloc(sizeof(int));
				sink_data->inbound_ids[0]= i;
				sink_data->rank = DEF_RANK;	
				p_array = add_page(p_array, sink_data);
			}
	
			/* Add inbounds/outbounds */	
			else
			{
				
				/* If its a link to itself, ignore the link */
				if(s_id != i)
				{
					p_array->array[i]->outbound_ids[j] = s_id;
					p_array = add_inbound_link(p_array, s_id, i);
					outbound_size += 1;
				}
			}

			/* Free up outbound URL, not needed anymore (have ID) */
			free(p_array->array[i]->outbound_urls[j]);
		}
		p_array->array[i]->outbound_count = outbound_size;
		free(p_array->array[i]->outbound_urls);
	}

	/* Add outbound links for page sinks */
	p_array = add_all_outbounds(p_array, orig_end);
	return p_array;
}

struct page_array * add_inbound_link(page_array * p_array, int curr_id, int in_id)
{
	int count = p_array->array[curr_id]->inbound_count;

	/* Increase size of array */
	if(count > 1)	
		p_array->array[curr_id]->inbound_ids = 
			realloc(p_array->array[curr_id]->inbound_ids, sizeof(int)*count);
	
	/* Insert new ID */
	p_array->array[curr_id]->inbound_ids[count-1] = in_id;
	if(p_array->array[curr_id]->inbound_ids[0] != -1)
		p_array->array[curr_id]->inbound_count += 1;
	return p_array;
}

struct page_array * calc_page_rank(page_array * p_array)
{
	int iteration, i, j, o_id, o_count;
	double total_rank, out_link_rank;

	/* Calculation iterations */
	for(iteration = 10; iteration > 0; iteration--)
	{
		for(i = 0; i < p_array->count; i++)
		{
			total_rank = p_array->array[i]->rank;
			/* get total inbound link rank */
			out_link_rank = 0.00;
			for(j = 0; j < (p_array->array[i]->inbound_count-1); j++)
			{
				o_id = p_array->array[i]->inbound_ids[j];
				if(o_id == i)
					continue;
				o_count = p_array->array[o_id]->outbound_count;
				out_link_rank += (double) (p_array->array[o_id]->rank/o_count);
			}	
			
			/* Get total pagerank */
			total_rank = (double)(((1-D_FACTOR)/p_array->count)+
				(D_FACTOR*out_link_rank));
			p_array->array[i]->rank = total_rank;
		}
	}
	return p_array;		
}

int search_id(page_array * p_array, char * search)
{
	int middle, i, j;
	middle = p_array->count/2;
	j = p_array->count-1;

	/* Search for URL starting from start and finish of array */
	for(i = 0; i < middle || j >= middle; i++)
	{
		if(strcmp(p_array->array[i]->url, search) == 0)
			return i;
		if(strcmp(p_array->array[j]->url, search) == 0)
			return j;
		j--;
		if(i > j)
			return -1;
	}
	return -1;
}

struct page_array * add_all_outbounds(page_array * p_array, int start)
{
	int i, j;
	int * full_array;
	full_array = malloc(sizeof(int)*p_array->count);
	/* Create int array with all IDS */
	for(j = 0; j < p_array->count; j++)
	{		
		full_array[j] = j;
	}

	/* Add links to/from rank sinks */
	for(i = 0; i < p_array->count; i++)
	{
		if( i >= start)
		{
			p_array->array[i]->outbound_count = p_array->count;
			p_array->array[i]->outbound_ids = malloc(sizeof(int)*p_array->count);
			p_array->array[i]->outbound_ids = full_array;

		}

		/* Adding inbound links from page sinks */
		for(j = start; j < p_array->count; j++)
		{
			p_array = add_inbound_link(p_array, i, j);
		}
	}	
	return p_array;
}

void free_rank(page_array * p_array)
{
	int i;

	/* Free all alloc'd array memory */
	for(i = 0; i < p_array->count; i++)
	{
		free(p_array->array[i]->url);
		free(p_array->array[i]->inbound_ids);
		free(p_array->array[i]);
	}
	free(p_array->array);
	free(p_array);
	return;
}

