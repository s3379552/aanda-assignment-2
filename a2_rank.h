#ifndef A2_RANK_H
#define A2_RANK_H
#include "a2.h"

/* Pagerank array struct definitions */
typedef struct page_data{
	int id;
	char * url;
	double rank;
	char ** outbound_urls;
	int outbound_count;
	int * outbound_ids;
	int inbound_count;
	int * inbound_ids;
}page_data;

typedef struct page_array{
	int count;
	page_data ** array;
}page_array;


/* Function to initialise pagerank array.
 * Returns newly created array. */
struct page_array * init_rank_array();

/* Function to add new page URL to array.
 * Extends array size by 1 and adds new URL onto end of array. This position
 * is the same value as its ID.*/
struct page_array * add_page(page_array * p_array, page_data * new_data);

/* Function to add all outbound (and inbound) links to all URLs in array.
 * Iterate through array from start to original ending position of the array.
 * For every element, iterate through its outbound link URLS. For each of these
 * links, search the array for the URL. If it exists, add the URL's ID to the 
 * current page URL's list of outbound IDs, and add the current URL's ID to the
 * link's own list of inbound IDs. Otherwise, the link URL doesn't currently exist
 * in the pagerank array, so add it to the end of the array and give it this position
 * as its ID. Then, add inbound and outbound links as in previous case.
 * Once the original array end position is reached, all following URLS are URLs that
 * had no outbound links (were not featured in the page URL column of the file). 
 * These are rank sinks, so add all page IDs as their outbound URLs. */
struct page_array * add_outbound_links(page_array * p_array);

/* Function for adding an inbound link to the specified URL.
 * The function allocates memory for an extra inbound URL in the specified URL,
 * then adds the other specified URL ID as the inbound link.*/
struct page_array * add_inbound_link(page_array * p_array, int curr_id, int in_id);

/* Function for calculating pagerank of all URLS.
 * Function calculates each pagerank 10 times to stabalise values.
 * For each URL, add up the sum of all the pageranks of its inbound ID's. If it
 * comes across its own ID, ignore it. Then, divide this value by the total number
 * of outbound links from the URL. Then, Calculate the final page rank for this iteration
 * using the formula specified in the assignment sheet. */
struct page_array * calc_page_rank(page_array * p_array);

/* Function to search for the given URL in the array.
 * Move from the top and bottom of the array inwards. If either position tracker finds
 * the given URL, return the URL's ID. Otherwise return -1.*/
int search_id(page_array * p_array, char * search);

/* Function to add all outbound links to page sinks.
 * Create a outbound link array to use on all page sinks, which is all IDS in the array.
 * Then, if the pointer is currently passing across a page sink, add this outbound array as
 * its outbound IDS. In all cases, add all the page sinks to the inbound IDs of it.*/
struct page_array * add_all_outbounds(page_array * p_array, int start);

/* Function to free all memory from the array. */
void free_rank(page_array * p_array);

#endif
