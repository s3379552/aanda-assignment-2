a2: a2.c a2.h a2_io.o a2_rank.o a2_index.o a2_pages.o
	gcc -g -ansi -Wall -pedantic -lm a2.c a2_io.o a2_rank.o a2_index.o a2_pages.o -o a2
a2_io.o: a2_io.c a2_io.h
	gcc -g -ansi -Wall -pedantic -lm -c a2_io.c
a2_rank.o: a2_rank.c a2_rank.h
	gcc -g -ansi -Wall -pedantic -lm -c a2_rank.c
a2_index.o: a2_index.c a2_index.h
	gcc -g -ansi -Wall -pedantic -lm -c a2_index.c
a2_pages.o: a2_pages.c a2_pages.h
	gcc -g -ansi -Wall -pedantic -lm -c a2_pages.c
clean: 
	rm -f a2 *.o
