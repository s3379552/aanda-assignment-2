#include "a2_pages.h"

struct name_array * init_array(page_array * p_array)
{
	int i;
	name_array * n_array = malloc(sizeof(name_array));
	n_array->count = p_array->count;
	n_array->array = calloc(p_array->count, sizeof(temp_node));
	
	/* Copy all ID's and URL's from pagerank array to name array */	
	for(i = 0; i < n_array->count; i++)
	{
		n_array->array[i] = calloc(1, sizeof(temp_node));
		n_array->array[i]->t_id = p_array->array[i]->id;
		n_array->array[i]->url = malloc((strlen(p_array->array[i]->url)+1)*sizeof(char));
		strcpy(n_array->array[i]->url, 
			p_array->array[i]->url);
	}
	return n_array;
}

struct docs_array * init_docs()
{
	/* Create temporary docs array */
	docs_array * d_array = malloc(sizeof(struct docs_array));
	d_array->count = 0;
	d_array->array = calloc(1, sizeof(struct temp_doc)); 
	return d_array;
}

void new_doc(docs_array ** d_array, temp_doc * new_doc)
{
	/* Inserting into head of array */
	if((*d_array)->count == 0)
	{
		(*d_array)->array[0] = new_doc;
		(*d_array)->count = 1;
	}

	/* Insert at end of array */
	else
	{
		(*d_array)->count += 1;
		(*d_array)->array = realloc((*d_array)->array, sizeof(struct temp_doc)*((*d_array)->count));
		(*d_array)->array[((*d_array)->count)-1] = new_doc;
	}
	return;
}

void sort_array(name_array ** u_array, int left, int right)
{
			
	int i, j, piv_id;
	char * pivot;
	i = left; 
	j = right;
	
	/* Calculate middle pivot value */
	piv_id = (int)(floor((double)(right-left)/2) + left);
	pivot = (*u_array)->array[piv_id]->url;

	while (i <= j)
	{

		/* Find two out of position nodes on either side of pivot */
		while(strcmp((*u_array)->array[i]->url, pivot) < 0)
			i++;
		while(strcmp((*u_array)->array[j]->url, pivot) > 0)
			j--;

		/* Swap nodes */
		if(i <= j)
		{
			swap_nodes(&((*u_array)->array[i]), &((*u_array)->array[j])); 
			i++;
			j--;	
		}
	}
	
	/* Sort both remaining sides recursively */
	if(left < j)
		sort_array(u_array, left, j);
	if(i < right)
		sort_array(u_array, i, right);
	return;
}

void swap_nodes(temp_node ** node1, temp_node ** node2)
{

	/* Swap nodes position */
	temp_node * t_node;
	t_node = *node1;
	*node1 = *node2;
	*node2 = t_node;
	return;
}

int find_url(name_array * n_array, char * s_url, int max)
{
	int middle, first, last;
	first = 0;
	last = max;

	/* Find middle of whole array */
	middle = (int)floor((float)(((last-first)/2.0)) + first);
	while(last >= first)
	{
	
		/* Compare request with middle */
		if(strcmp(n_array->array[middle]->url, s_url) == 0)
			return n_array->array[middle]->t_id;
		else if(strcmp(s_url, n_array->array[middle]->url) < 0)
			last = middle - 1;
		else
			first = middle + 1;

		/* Find new middle value */
		middle = (int)floor((float)(((last-first)/2.0)) + first);
	}
	return -1;
	
}

struct index_table * insert_terms(name_array * u_array, docs_array * d_array, index_table * i_table, page_array * p_array)
{
	int i, new_id;
	doc_entry * new_entry;

	/* Insert all doc frequency nodes */
	for(i = 0; i < d_array->count; i++)
	{
		new_entry = calloc(1, sizeof(struct doc_entry));

		/* Find URL's ID */	
		new_id =  find_url(u_array, d_array->array[i]->url, u_array->count-1);
		
		/* Missing ID Handling */
		if(new_id == -1)
		{
			printf("URL NOT FOUND?!\n");
			printf("%s\n", d_array->array[i]->url);
			continue;
		}

		/* Create new doc frequency node for insertion into index */
		new_entry->doc_id = new_id;		
		new_entry->word_freq = 1;
		i_table = index_insert(i_table, p_array, new_entry, d_array->array[i]->keyword);
		
		/* Free temporary node information */
		free(d_array->array[i]->url);
		free(d_array->array[i]->keyword);
		free(d_array->array[i]);	
	}

	/* Free temporary doc array */
	free(d_array->array);
	free(d_array);
	return i_table;
}

void free_names(name_array * n_array)
{
	int i;

	/* Free names array */
	for(i = 0; i < n_array->count; i++)
	{
		free(n_array->array[i]->url);
		free(n_array->array[i]);
	}
	free(n_array->array);
	free(n_array);
	return;
}
